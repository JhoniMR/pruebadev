import { Component } from '@angular/core';
import { ServiceService } from './service.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Prueba';

  public mostrarCodigo: boolean = false;

 /* public changeClick(){
   // this.mostrarCodigo = !this.mostrarCodigo;
  }*/

  public changeRadio(event){  
    console.log(event);
    console.log("event",event.target.value)
    let valor = event.target.value;

    if(valor == "si"){
      this.mostrarCodigo = true
    }else{
      this.mostrarCodigo = false
    }
  }

  
  public personas: Array<any>[]
  public formulario: Array<any>[] 

  constructor(public json: ServiceService){
  this.json.getJson('http://138.197.118.88/datacity-pruebas/public/users/service/get/subscribers').subscribe((res:any) =>{
    console.log(res);
    this.personas = res.data;
  })

  this.json.getJson('http://138.197.118.88/datacity-pruebas/public/users/service/get/formsPublicados').subscribe((res:any) =>{
    console.log(res);
    this.formulario = res.data;
  })

  }

  public alerta (){
    alert('Los datos han sido guardados')
  }
}
